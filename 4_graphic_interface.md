gui
---
```
pacman -S cinnamon
```

set auto-start
--------------
```
echo exec cinnamon-session-cinnamon2d > ~/.xinitrc
```

other gui tools
---
```
# for open archive in gui
pacman -S cfile-roller
```
