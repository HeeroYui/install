Multimedia section
==================

Install players
---------------

```
pacman -S vlc
```

Encode DVD
----------

```
pacman -S handbrake libdvdread libdvdcss libdvdnav
```  

Transcode TV
------------

```
pacman -S ffmpeg
```  


  
